enum ForbiddenAreas
{
	AREA_VIP_MALL = 3630,  // Oshu'gun
	AREA_VIP_MALL_TWO = 3631,  // Oshu'gun
};

class map_security : public PlayerScript
{
public:
	map_security() : PlayerScript("map_security") {}

	void OnUpdateZone(Player* pPlayer, uint32 newZone, uint32 newArea)
	{
		// Forbidden areas:
		switch (pPlayer->GetAreaId())
		{
		case AREA_VIP_MALL:
		{
			if (pPlayer->GetSession()->GetSecurity() >= 1)
				return;

			pPlayer->TeleportTo(870, 4332.155273f, 2805.197754f, 54.559120f, 2.488806f); //Mall
			pPlayer->GetSession()->SendAreaTriggerMessage("You don't have VIP access to reach this destination.");
		}
		break;

		case AREA_VIP_MALL_TWO:
		{
			if (pPlayer->GetSession()->GetSecurity() >= 2)
				return;

			pPlayer->TeleportTo(870, 4332.155273f, 2805.197754f, 54.559120f, 2.488806f); //Mall
			pPlayer->GetSession()->SendAreaTriggerMessage("You don't have VIP access to reach this destination.");
		}
		break;
		}
	}
};

void AddSC_map_security()
{
	new map_security;
}


class Vip_Mall_Trigger : public CreatureScript
{
public: 
	Vip_Mall_Trigger() : CreatureScript("Vip_Mall_Trigger") {}

	struct Vip_Mall_TriggerAI : public ScriptedAI
	{
		Vip_Mall_TriggerAI(Creature* creature) : ScriptedAI(creature) {}

		uint32 tickTimer;

		void Reset()
		{
			tickTimer == 1000;
		}

		void UpdatedAI(uint32 diff)
		{
			if (tickTimer <= diff)
			{
				if (Player* player = me->SelectNearestPlayer(10.0f))
				{
					player->TeleportTo(530, -2720.421387f, 8306.852539f, -83.080750f, 4.727089f);
				}

			}
			else
				tickTimer -= diff;
		}
	};

	CreatureAI * GetAI(Creature * creature) const
	{
		return new Vip_Mall_TriggerAI(creature);
	}
};

void AddSC_Vip_Mall_Trigger()
{
	new Vip_Mall_Trigger;
}
