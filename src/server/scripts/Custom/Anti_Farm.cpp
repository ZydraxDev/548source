/* 
Scriptname: anti_farm (For killstreak and future quests on ADVERSEWOW)
Use of it: Preventing FARMING of own characters -> Checks IP.
*/

/*
SQL =
CREATE TABLE `anti-farm log` (
  `ID` MEDIUMINT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`Character` TEXT NOT NULL,
	`Account` MEDIUMINT(1) UNSIGNED NULL DEFAULT '0',
	PRIMARY KEY (`ID`)
)
COMMENT='used by the Anti-Farm System'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DEFAULT
AUTO_INCREMENT=1
*/
#include "ScriptPCH.h"
#include "World.h"
#include "Language.h"
#include "WorldSession.h"
#include "Player.h"

class anti_farm : public PlayerScript
{
public: 
	anti_farm() : PlayerScript("anti_farm") { }
	void OnPVPKill(Player * killer, Player * killed)
	{
			if ((killer->GetSession()->GetRemoteAddress() == killed->GetSession()->GetRemoteAddress()) && (killer->GetGUID() != killed->GetGUID()))
			{
				// killer->GetSession()->SendNotification("[Anti-Farm System] Ban dang Farm honor, ban se mat Honor va Conquest Point cho hanh vi cua minh!");
				killer->ModifyCurrency(CURRENCY_TYPE_HONOR_POINTS, -50000, MODIFY_CURRENCY_NO_PRINT_LOG); // -500 honor
				killer->ModifyCurrency(CURRENCY_TYPE_CONQUEST_POINTS, -50000, MODIFY_CURRENCY_NO_PRINT_LOG); // -500 CP
				killer->DestroyItemCount(888002, 10, true);
			}
			else
			{ 
				return;
			}
	}
};

void AddSC_anti_farm()
{
	new anti_farm;
}